import json
import os.path

from flask import Flask, request
import glob
import json

app = Flask(__name__)


@app.route('/')
def connect():
    return "True"


@app.route('/settings')
def setting_server():
    data = request.form
    if "test" not in data.keys() or "validation" not in data.keys():
        return "test or validation path is missing"
    test_path = data["test"]
    validation_path = data["validation"]
    test = sum([1 for x in glob.glob("%s/*" % test_path)])
    validation = os.path.isfile(validation_path)
    return {"test_dir": test, "is_validation_ok": validation}

@app.route('/model-result-v1')
def model_result():  # put application's code here
    # data = request.form
    # if "test" not in data.keys() or "validation" not in data.keys():
    #     return "test or validation path is missing"
    # test_path = data["test"]
    # validation_path = data["validation"]
    # test = sum([1 for x in glob.glob("%s/*" % test_path)])
    # validation = sum([1 for x in glob.glob("%s/*" % validation_path)])
    # return {"test": test, "validation": validation}
    with open("valid.json", encoding="utf-8") as f:
        json_test = json.load(f.read())
        return json_test


@app.route('/start')
def start_server():  # put application's code here
    with open("valid.json", encoding="utf-8") as f:
        json_test = json.load(f.read())
        return json_test


if __name__ == '__main__':
    app.run()
