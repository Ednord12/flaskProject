import numpy as np
import tensorflow as tf
from PIL import Image


def predict(model, image):
    assert type(image) == np.ndarray
    proba = model(image)
    return proba


def generate_image_from_array(x_array):
    for index, x in enumerate(x_array):
        im = Image.fromarray(x)
        im.show()
        im.save("data/Image/{}.jpg".format(index))


if __name__ == "__main__":

    # Load model
    model = tf.keras.models.load_model('model/Image_RVL')
    print(model.summary())

    # Load data
    # x_test = np.load("data/test_x_40k_tif.npy")
    y_test = np.load("data/test_y_40k_tif.npy")

    # Generate image

    # predicted 
    for index, x in enumerate(y_test):
        x = Image.open("data/Image/{}.jpg".format(index))
        # x.show()
        x = np.asarray(x)
        x = np.expand_dims(x, axis=(0, -1))  # 1 , (224,224) , 1
        pred_proba = predict(model, x)[0]  # we task only first array because we have only one batch
        classe = np.argmax(pred_proba)
        print(y_test[index], classe)
