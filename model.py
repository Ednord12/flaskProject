import glob
import numpy as np


class Model:
    def __init__(self, test_dataset, validation_dataset):
        self.test_dataset = test_dataset
        self.validation_dataset = validation_dataset

    def evaluate(self):
        return [np.random.randint(16) for y in glob.glob("%s/*" % self.test_dataset)]
