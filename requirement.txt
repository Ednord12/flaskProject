PyQt5
Pillow==7.1.1
tqdm==4.46.0
tensorflow==2.2.0
Keras-Applications==1.0.8